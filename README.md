# Selfhosted Dashboard

A simple dashboard for selfhosted services using Jekyll.

## UI

Click the icon on the left to expand the text, and the icon on the right to go to the site.

// TODO: Screenshot

## Directory Structure

One can add services under the folder `_websites/`. See `esoj.md` for an example, and the section below for the documentation of YAML fields.

Images for the icons are placed under `assets/`.

The footer text is stored in `_footer.md`.

## YAML fields
- `title`: Title
- `subtitle`: Subtitle
- `link`: URL to the service
- `image`: Used as a button for the link on the right. Useful for, e.g. Netdata badges that shows whether the service is up.
- `icon`: The icon displayed on the left.
